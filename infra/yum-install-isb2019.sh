#!/bin/sh
echo adding more packages 
yum -y install epel-release
yum -y update
yum -y install \
	wget \
	sudo \
	lftp \
	curl \
	screen \
	tmux \
	rsync \
	sysstat \
	yum-utils \

yum -y groupinstall \
development \
gnome-desktop-environment \
compat-libraries \
fonts \
gnome-apps \
gnome-desktop \
input-methods \
internet-applications \
internet-browser \
java-platform \
legacy-x \
multimedia \
network-file-system-client \
networkmanager-submodules \
office-suite \
print-client \
security-tools \
x11 \
additional-devel \
platform-devel \
technical-writing \


yum -y install \
atlas-devel \
atlas-devel \
atlas-static \
bash-completion \
bind-utils \
blas-devel \
bzip2-devel \
cairo-devel \
cmake \
curl-devel \
elinks \
environment-modules \
eog \
evince \
fftw \
fftw-devel \
fftw-static \
gd-devel \
gedit \
gnome-backgrounds \
java-1.8.0-openjdk.x86_64 \
lapack-devel \
libicu-devel \
libreoffice \
libtiff-devel \
libxml2-devel \
libXt-devel.x86_64 \
nano \
ncurses-devel \
nedit \
openmpi-devel \
openssl-devel \
parallel \
pdsh-rcmd-ssh.x86_64 \
perl-digest \
perl-Digest-MD5 \
readline-devel \
tree \
vim-enhanced \
vim-X11 \
xpdf \
xz-devel \
zlib-devel \
zsh \
