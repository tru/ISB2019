mkdir -p /home/centos/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCfzXiaC4+k8XA9oX3QVxukFIC6KwC058Ow5ohLEKEkLZSJmUKQxM4yvdwbwQbtTBoPlhoAl5TWjDgbuoXWldRkoG9s9U8Gz7UmPmh+2kN5dRiSC96qRopHGWbRr/3DhlXaJM3ce50Cfa5rrTKDsQhA4VOUSOoA45q/oF8noCEn9AJ7Ap+NeL63Lb26Wj4yJf/5RpXoHdd9bNkgjpAAeM/e0YQ3hUaX/bh8Dts2kZa17UGxdly03H7TAWylutT08LOhDzOCrooRvrPcrGyiuEQ6kCU18HDlLeMD2Xm63JYAL4OqM7HXpCzfOkSw/q1YgD57ptuvSCcqhIj21adebbxv centos-admin'  > /home/centos/.ssh/authorized_keys
chmod 0700 /home/centos/.ssh
chmod 0600 /home/centos/.ssh/authorized_keys
chown -R centos:centos /home/centos/.ssh
restorecon -rv /home/centos
echo 'centos ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
restorecon -rv /etc/sudoers
