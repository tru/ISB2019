mkdir -p /home/isb2019/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6eiy3YKnNqZSzoUShNqXls8AnSVGv/mCWVh1fJKSGl9Pcb5I3IemhChAf4/93mBPmnNcsVJH666RlEA6ixj7PvjIDvtGY7+NX1+IFOkLLHleqfbN24keG/HGqvs5XyE+2m4ygAR2ZuN+L8XURRVNwITNPnIQO+Tk0vKxQd4Z3g8tryiygTj4BbVMQlpElBF31hqWsQAdivaF0dv1dOsrQo3nDq/E/fAFgmMDDcTMywL3OrM56MVUmRvtdiEGHkjioUu4d7UjhUXvGXrrOhCV9NXBIbR2JMsmE3YpPNNrVnn8A5hwpnE3uZHNKelNx/LWyQ1rXtfWVEMgBp9Chr/t1 isb2019-student' > /home/isb2019/.ssh/authorized_keys
chmod 0700 /home/isb2019/.ssh
chmod 0600 /home/isb2019/.ssh/authorized_keys
chown -R isb2019:isb2019 /home/isb2019/.ssh
restorecon -rv /home/isb2019
