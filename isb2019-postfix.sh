#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u


sudo sed -i -e 's/^root.*postfix/root: tru\@pasteur.fr/' /etc/aliases 
grep tru@pasteur.fr /etc/aliases|| (echo 'root: tru@pasteur.fr'| sudo tee -a /etc/aliases )
sudo /usr/bin/newaliases
postconf | grep message_size_limit
sudo postconf -e 'inet_protocols = ipv4'
sudo postconf -e 'message_size_limit = 20480000' 
sudo postconf -e 'relayhost = [smtp.pasteur.fr]'
sudo postconf -e 'mydestination = '
sudo postconf -e 'virtual_alias_maps = regexp:/etc/postfix/virtual'
cd /etc/postfix && curl https://gitlab.pasteur.fr/tru/ISB2019/raw/master/postfix-virtual-isb2019.patch  | sudo patch && \
sudo postmap virtual
sudo /sbin/service postfix restart
postconf | grep message_size_limit
date| mail -s `hostname` tru
date| mail -s `hostname` tru@`hostname`
echo | mail -s root:`hostname` root

